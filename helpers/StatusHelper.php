<?php

/*
 * Junati Innovations Pvt Ltd.
 * Project: UIMS
 * Module: University Core Module
 * 
 */

namespace samarth\helpers;

use yii\base\Component;
use yii\helpers\Html;

class StatusHelper extends Component
{

    const INACTIVE = 0;
    const ACTIVE = 1;
    const NO = 0;
    const YES = 1;
    const INDIAN_NATIONALITY = 1;
    const OTHER_NATIONALITY = 2;

    public static function getStatus()
    {
        return [self::INACTIVE => 'In-active', self::ACTIVE => 'Active'];
    }

    public static function resolveStatus($id)
    {
        if (array_key_exists($id, self::getStatus())) {
            return self::getStatus()[$id];
        } else {
            return '';
        }
    }


    public static function getYesNo()
    {
        return [self::NO => 'No', self::YES => 'Yes'];
    }

    public static function resolveYesNo($id)
    {

        if (array_key_exists($id, self::getYesNo())) {
            return self::getYesNo()[$id];
        } else {
            return '';
        }
    }


    public static function getBloodGroup()
    {
        return [
            'O-' => 'O-',
            'O+' => 'O+',
            'A-' => 'A-',
            'A+' => 'A+',
            'B-' => 'B-',
            'B+' => 'B+',
            'AB-' => 'AB-',
            'AB+' => 'AB+',
        ];
    }

    public static function resolveBloodGroup($id)
    {
        if (array_key_exists($id, self::getBloodGroup())) {
            return self::getBloodGroup()[$id];
        } else {
            return '';
        }
    }

    public static function getReligion()
    {
        return [
            '1' => 'Hindu',
            '2' => 'Muslim',
            '3' => 'Sikh',
            '4' => 'Christian',
            '5' => 'Jain',
            '6' => 'Other'
        ];
    }

    public static function resolveReligion($id)
    {
        if (array_key_exists($id, self::getReligion())) {
            return self::getReligion()[$id];
        } else {
            return '';
        }
    }


    public static function getNationality()
    {
        return [self::INDIAN_NATIONALITY => 'Indian', self::OTHER_NATIONALITY => 'Other'];
    }

    public static function resolveNationality($key)
    {
        if (array_key_exists($key, self::getNationality())) {
            return self::getNationality()[$key];
        } else {
            return "";
        }
    }

    public static function getLastUpdatedTimeofTable($table)
    {
        date_default_timezone_set("Asia/Kolkata");
        try {
            $model = $table::find()->select('updated_at')->orderBy('updated_at Desc')->one();
        } catch (\Exception $e) {
            $model = $table::find()->select('created_at')->orderBy('created_at Desc')->one();
        }

        if (!empty($model)) {
            if (!empty($model->updated_at)) {
                $diff = time() - $model->updated_at;
            } else {
                $diff = time() - $model->created_at;
            }

            $dtF = new \DateTime('@0');
            $dtT = new \DateTime("@$diff");
            return $dtF->diff($dtT)->format("%a days, %h hours, %i minutes and %s seconds");
        } else {
            return 0;
        }
    }
    public static function getLastUpdatedRow($table,$row)
    {
        date_default_timezone_set("Asia/Kolkata");
        try {
            $model = $table::find()->select('updated_at')->where(['id'=>$row])->orderBy('updated_at Desc')->one();
        } catch (\Exception $e) {
            $model = $table::find()->select('created_at')->where(['id'=>$row])->orderBy('created_at Desc')->one();
        }

        if (!empty($model)) {
            if (!empty($model->updated_at)) {
                $diff = time() - $model->updated_at;
            } else {
                $diff = time() - $model->created_at;
            }

            $dtF = new \DateTime('@0');
            $dtT = new \DateTime("@$diff");
            return $dtF->diff($dtT)->format("%a days, %h hours, %i minutes and %s seconds");
        } else {
            return 0;
        }
    }


}