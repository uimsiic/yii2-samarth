<?php

namespace samarth\helpers;

//use app\modules\rti\models\MenuItems;
use yii\base\Component;

class MenuHelper extends Component
{
    public static $result_set = [];

    public static function getMenuItems($employee_id)
    {
        self::getElementsInArray(\app\modules\rti\models\MenuItems::getItems($employee_id));
        self::getElementsInArray(\app\modules\legal\models\MenuItems::getItems($employee_id));
        return self::$result_set;
    }

    private static function getElementsInArray($items)
    {
        foreach ($items as $key => $item) {
            array_push(self::$result_set, $item);
        }
    }
}