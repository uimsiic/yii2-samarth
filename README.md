Helpers
=======================================
This is a library for providing all the helper classes.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist samarth/yii2-samarth "*"
```

or add

```
"samarth/yii2-samarth": "*"
```

to the require section of your `composer.json` file.


Installation From Bitbucket
---------------------------

Add

```
"samarth/yii2-samarth": "dev-master"
```

to the require section of your `composer.json` file and 

Add

```php

"repositories": [
    {
        "type": "git",
        "url": "https://vishal_iic@bitbucket.org/uimsiic/yii2-samarth.git"
    }
]
```

at the end of your `composer.json` file.

**Note: replace `vishal_iic` with your user name.**